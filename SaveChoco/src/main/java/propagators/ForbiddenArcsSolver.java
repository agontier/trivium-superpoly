/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2021, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package propagators;

import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.graphsolver.variables.GraphEventType;
import org.chocosolver.graphsolver.variables.delta.GraphDeltaMonitor;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.events.PropagatorEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.procedure.PairProcedure;
import org.jgrapht.alg.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static org.chocosolver.util.ESat.*;

/**
 * This class is an adapted version of a SAT solver where a literal is an arc
 * and a clause is disjunction of forbidden arcs.
 * As long as at least two forbidden arcs are still in potential graph, nothing to do.
 * These arcs are called 'watched arcs'.
 * As soon as one becomes mandatory, it looks for a new watched arc.
 * If there is none, the last one watched is removed from the graph.
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 16/03/2021
 */
public class ForbiddenArcsSolver extends Propagator<DirectedGraphVar> {

    // List of problem addClauses.
    public ArrayList<Clause> clauses = new ArrayList<>();
    // 'watches_[lit]' is a list of constraints watching 'lit'(will go
    // there if literal becomes true).
    private final HashMap<Literal, ArrayList<Watcher>> watches_ = new HashMap<>();
    // Assignment stack; stores all assigments made in the order they
    // were made.
    ArrayList<Literal> trail_ = new ArrayList<>();
    // Head of queue(as index into the trail_.
    int qhead_;


    DirectedGraphVar graph;
    private final GraphDeltaMonitor gdm;
    private final PairProcedure onEvent;

    public ForbiddenArcsSolver(DirectedGraphVar g) {
        super(new DirectedGraphVar[]{g}, PropagatorPriority.BINARY, true);
        this.graph = g;
        this.gdm = g.monitorDelta(this);
        this.onEvent = (i, j) -> {
            Literal pp = new Literal(i, j);
            if (watches_.containsKey(pp)) {
                trail_.add(pp);
            }
            forcePropagate(PropagatorEventType.CUSTOM_PROPAGATION);
        };
    }


    @Override
    public int getPropagationConditions(int vIdx) {
        return GraphEventType.ADD_ARC.getMask();
    }

    @Override
    public void propagate(int i, int mask) throws ContradictionException {
        gdm.freeze();
        if (GraphEventType.isAddArc(mask))
            gdm.forEachArc(onEvent, GraphEventType.ADD_ARC);
        gdm.unfreeze();
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        try {
            while (qhead_ < trail_.size()) {
                Literal p = trail_.get(qhead_++);
                propagateClauses(p);
            }
        } finally {
            qhead_ = 0;
            trail_.clear();
        }
    }

    @Override
    public final ESat isEntailed() {
        cl:
        for (Clause c : clauses) {
            boolean u = false;
            for (int i = 0; i < c.literals_.length; i++) {
                ESat b = valueLit(c._g(i));
                if (b == TRUE) {
                    continue cl;
                } else if (b == UNDEFINED) {
                    u = true;
                }
            }
            return u ? UNDEFINED : FALSE;
        }
        return TRUE;
    }

    // Add a clause to the solver.
    public void addClause(Literal[] ps) {
        int j = 0;
        for (int i = 0; i < ps.length; i++) {
            if (valueLit(ps[i]) == TRUE) {
                return;
            } else if (valueLit(ps[i]) != FALSE) {
                ps[j++] = ps[i];
            }
        }
        if (j < ps.length) {
            Literal[] tmp = ps;
            ps = new Literal[j];
            System.arraycopy(tmp, 0, ps, 0, j);
        }
        Clause cr = new Clause(ps);
        clauses.add(cr);
        attachClause(cr);
    }

    // Add a clause to the solver.
    public void addClause(Pair<Integer, Integer>[] forbiden) {
        Literal[] ps = new Literal[forbiden.length];
        for (int i = 0; i < forbiden.length; i++) {
            ps[i] = new Literal(forbiden[i].getFirst(), forbiden[i].getSecond());
        }
        addClause(ps);
    }

    // Attach a clause to watcher lists.
    private void attachClause(Clause cr) {
        assert cr.size() > 1;
        ArrayList<Watcher> l0 = watches_.computeIfAbsent(cr._g(0), k -> new ArrayList<>());
        ArrayList<Watcher> l1 = watches_.computeIfAbsent(cr._g(1), k -> new ArrayList<>());
        l0.add(new Watcher(cr, cr._g(1)));
        l1.add(new Watcher(cr, cr._g(0)));
    }

    private void propagateClauses(Literal p) throws ContradictionException {
        assert valueLit(p) == FALSE;
        // 'p' is enqueued fact to propagate.
        ArrayList<Watcher> ws = watches_.get(p);

        int i = 0;
        int j = 0;
        while (ws != null && i < ws.size()) {
            // Try to avoid inspecting the clause:
            Literal blocker = ws.get(i).blocker;
            if (valueLit(blocker) == ESat.TRUE) {
                ws.set(j++, ws.get(i++));
                continue;
            }

            // Make sure the false literal is data[1]:
            Clause cr = ws.get(i).clause;
            if (cr._g(0).equals(p)) {
                cr._s(0, cr._g(1));
                cr._s(1, p);
            }
            assert (cr._g(1).equals(p)) : "expected " + p + " found " + cr._g(1);
            assert valueLit(cr._g(1)) == FALSE;
            i++;

            // If 0th watch is true, then clause is already satisfied.
            final Literal first = cr._g(0);
            Watcher w = new Watcher(cr, first);
            if (first != blocker && valueLit(first) == ESat.TRUE) {
                ws.set(j++, w);
                continue;
            }

            // Look for new watch:
            boolean cont = false;
            for (int k = 2; k < cr.size(); k++) {
                if (valueLit(cr._g(k)) != ESat.FALSE) {
                    cr._s(1, cr._g(k));
                    cr._s(k, p);
                    ArrayList<Watcher> lw = watches_.computeIfAbsent(cr._g(1), k1 -> new ArrayList<>());
                    lw.add(w);
                    cont = true;
                    break;
                }
            }

            // Did not find watch -- clause is unit under assignment:
            if (!cont) {
                ws.set(j++, w);
                if (valueLit(first) == FALSE) {
                    // Copy the remaining watches_:
                    while (i < ws.size()) {
                        ws.set(j++, ws.get(i++));
                    }
                    ws.subList(j, ws.size()).clear();
                }/* else {
                    trail_.add(first);
                }*/
                graph.removeArc(first.p0, first.p1, this);
            }
        }
        if (ws != null) {
            if (ws.size() > j) {
                ws.subList(j, ws.size()).clear();
            }
        }
        checkWatch(p);
    }

    private void checkWatch(Literal p) {
        ArrayList<Watcher> ws = watches_.get(p);
        if (ws != null && ws.size() > 1) {
            long cnt = ws.stream().map(e -> e.clause).distinct().count();
            assert cnt == ws.size();
        }
    }


    // The current value of a literal.
    ESat valueLit(Literal lit) {
        int s = lit.p0;
        int d = lit.p1;
        if (graph.getMandSuccOf(s).contains(d)) {
            // arc is in kernel
            return ESat.FALSE;
        } else if (!graph.getPotSuccOf(s).contains(d)) {
            // arc not in envelop
            return ESat.TRUE;
        }
        return ESat.UNDEFINED;
    }

    /**
     * Clause -- a simple class for representing a clause
     */
    static class Clause {
        // 2 dim array
        private final Literal[] literals_;

        Clause(Literal[] ps) {
            literals_ = ps.clone();
        }

        public int size() {
            return literals_.length;
        }

        public Literal _g(int i) {
            return literals_[i];
        }

        void _s(int pos, Literal l) {
            literals_[pos] = l;
        }

        @Override
        public String toString() {
            return "Clause{" +
                    "literals_=" + Arrays.toString(literals_) +
                    '}';
        }
    }

    /**
     * A watcher represent a clause attached to a literal.
     */
    static class Watcher {

        Clause clause;
        Literal blocker;

        Watcher(final Clause cr, Literal l) {
            this.clause = cr;
            this.blocker = l;
        }
    }

    static class Literal {
        int p0, p1;

        public Literal(int p0, int p1) {
            if (p0 < p1) {
                this.p0 = p0;
                this.p1 = p1;
            } else {
                this.p0 = p1;
                this.p1 = p0;
            }
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (!(obj instanceof Literal))
                return false;
            return p0 == ((Literal) obj).p0 && p1 == ((Literal) obj).p1;
        }

        public int hashCode() {
            return p0 + 31 * p1;
        }

        @Override
        public String toString() {
            return p0 + "->" + p1;
        }
    }
}
