/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package propagators;

import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.graphsolver.variables.GraphEventType;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.Variable;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.graphs.DirectedGraph;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 25/11/2020
 */
public class PropReachableIV2 extends Propagator<Variable> {

    final DirectedGraphVar graph;
    final DirectedGraph d; // doublies
    final IntVar[] RIV;
    private final int sink;


    public PropReachableIV2(DirectedGraphVar g, DirectedGraph doublements, IntVar[] reachableIV, int sink) {
        super(new DirectedGraphVar[]{g}, PropagatorPriority.LINEAR, false);
        this.graph = g;
        this.sink = sink;
        this.d = doublements;
        this.RIV = reachableIV;
    }

    public void updatelb(int i) throws ContradictionException {
        if (i != sink && i != 0) {
            int minlb = 9999;
            int maxub = 0;
            for (int p : graph.getPotPredOf(i)) {
                if (!d.arcExists(p, i)) {
                    minlb = Math.min(minlb, RIV[p].getLB());
                    maxub = Math.max(maxub, RIV[p].getUB());
                } else {//doubling arc
                    int s4 = d.getSuccOf(p).max();
                    int s5 = d.getSuccOf(p).min();
                    if (graph.getPotSuccOf(p).contains(s4) && graph.getPotSuccOf(p).contains(s5)) {
                        if (i == s4) {
                            minlb = Math.min(minlb, RIV[p].getLB() - RIV[s5].getUB());
                            maxub = Math.max(maxub, RIV[p].getUB() - RIV[s5].getLB());
                        } else if (i == s5) {
                            minlb = Math.min(minlb, RIV[p].getLB() - RIV[s4].getUB());
                            maxub = Math.max(maxub, RIV[p].getUB() - RIV[s4].getLB());
                        }
                    }
                }
            }
            if (RIV[i].getUB() < minlb || RIV[i].getLB() > maxub) {
                graph.removeNode(i, this);
            } else {
                RIV[i].updateBounds(minlb, maxub, this);
                if (minlb > 0)cheklb(i);
            }
        }
    }

    private void cheklb(int i) throws ContradictionException {
        int lb = RIV[i].getLB();
        for (int s : graph.getPotSuccOf(i)) {
            if (!d.arcExists(i, s) && RIV[s].getUB() < lb) {
                graph.removeArc(i, s, this);
            }
        }
        // check doublies
        if (d.getSuccOf(i).size() > 0) {
            int s4 = d.getSuccOf(i).max();
            int s5 = d.getSuccOf(i).min();
            // to check
            if (graph.getPotSuccOf(i).contains(s4) && graph.getPotSuccOf(i).contains(s5)) {
                if (RIV[s4].getUB() + RIV[s5].getUB() < lb) {
                    graph.removeArc(i, s4, this);
                    graph.removeArc(i, s5, this);
                }
            }
        }
    }

    @Override
    public int getPropagationConditions(int vIdx) {
        return GraphEventType.REMOVE_NODE.getMask();
    }


    @Override
    public void propagate(int evtmask) throws ContradictionException {
        for (int i : graph.getPotentialNodes()) {
            updatelb(i);
        }
    }


    @Override
    public ESat isEntailed() {
        return ESat.TRUE;
    }
}
