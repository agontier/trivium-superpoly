/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */

import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import org.chocosolver.graphsolver.GraphModel;
import org.chocosolver.graphsolver.search.strategy.GraphSearch;
import org.chocosolver.graphsolver.search.strategy.GraphStrategy;
import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.graphsolver.variables.GraphVar;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Settings;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.strategy.AbstractStrategy;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.SetVar;
import org.chocosolver.util.objects.graphs.DirectedGraph;
import org.chocosolver.util.objects.graphs.Orientation;
import org.chocosolver.util.objects.setDataStructures.SetType;
import org.jgrapht.alg.util.Pair;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import propagators.*;
import utils.Reg;
import utils.Registre;

import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * <br/>
 *
 * @author Charles Prud'homme, Arthur Gontier
 * @since 24/11/2020
 */
public class TriviumGraph {

    private static class Shift {
        int[] shift;
        Reg[] regis;

        public Shift(int[] shift, Reg[] regis) {
            this.shift = shift;
            this.regis = regis;
        }
    }

    private static final HashMap<Reg, Shift> SHIFTS = new HashMap<>();

    static {
        SHIFTS.put(Reg.A, new Shift(new int[]{69, 66, 111, 110, 109}, new Reg[]{Reg.A, Reg.C, Reg.C, Reg.C, Reg.C}));
        SHIFTS.put(Reg.B, new Shift(new int[]{78, 66, 93, 92, 91}, new Reg[]{Reg.B, Reg.A, Reg.A, Reg.A, Reg.A}));
        SHIFTS.put(Reg.C, new Shift(new int[]{87, 69, 84, 83, 82}, new Reg[]{Reg.C, Reg.B, Reg.B, Reg.B, Reg.B}));
    }

    @Option(name = "-r",
            aliases = {"--nb-round"},
            usage = "Nombre de tours (default: 672).",
            required = true)
    protected int nbRound;

    @Option(name = "-i",
            aliases = {"--instance"},
            usage = "Instance à charger (default: iv_672_1).",
            required = true)
    private Instance instance;

    @Option(name = "-b",
            aliases = {"--bit"},
            usage = "Dernier bit à 1 (default: all bits).",
            required = true)
    protected Last last; //last bit set to 1 (source of the graph)

    @Option(name = "-dd",
            aliases = {"--debug"},
            usage = "Debug multi-thread")
    protected boolean debugPara = false;

    private final TObjectIntHashMap<Registre> regNode;
    private final TIntObjectHashMap<Registre> nodReg;
    private int currentNode;
    private final PriorityQueue<Registre> toConnect;
    private DirectedGraph doublements;//only doubling arcs
    private DirectedGraph longs;//only longs arcs
    private DirectedGraph loops;//only looping arcs
    private final int n; // nb of nodes max

    private final SetType dftSetType = SetType.RANGESET;
    private int source, sink;

    private DirectedGraphVar g;
    private IntVar[] outDegrees;
    private IntVar[] reachableIV;
    private Set<Integer> IV;

    /**
     * collect parralell solutions and filter doublies
     */
    public static class Soljoin {
        public ArrayList<int[]> sols;
        public int doublons;
        public int totalsol;

        public Soljoin() {
            sols = new ArrayList<>();
            doublons = 0;
            totalsol = 0;
        }

        private void soladd(int[] sol) {
            this.sols.add(sol);
        }

        private void soladdfilter(int[] sol, ArrayList<int[]> sols) {
            int[] removesol = new int[0];
            for (int[] sol2 : sols)
                if (sol2.length == sol.length) {
                    boolean eq = true;
                    for (int i : sol) {
                        boolean in = false;
                        for (int i2 : sol2)
                            in = in || i == i2;
                        eq = eq && in;
                    }
                    if (eq) removesol = sol2;
                }
            if (removesol.length == 0) sols.add(sol);
            else {
                sols.remove(removesol);
                doublons += 2;
            }
        }

        private void filterdoublies() {
            ArrayList<int[]> newsols = new ArrayList<>();
            for (int[] sol : sols)
                soladdfilter(sol, newsols);
            this.sols = newsols;
        }

        private void soladdall(ArrayList<int[]> sols) {
            sols.forEach(this::soladd);
        }
    }

    // Debug parallelism
    public final AtomicInteger remaining = new AtomicInteger(0);
    public final AtomicInteger active = new AtomicInteger(0);
    public BitSet[] gmatrix;
    public BitSet[] dmatrix;

    /**
     * model and solve trivium as a graph
     */
    public TriviumGraph(String... args) throws CmdLineException {
        CmdLineParser cmdparser = new CmdLineParser(this);
        try {
            cmdparser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("TriviumWithGraph -r <round> -i <instance> -b <bit>");
            cmdparser.printUsage(System.err);
            throw e;
        }
        this.n = nbRound * 3 + 300;
        this.regNode = new TObjectIntHashMap<>();
        this.nodReg = new TIntObjectHashMap<>();
        this.toConnect = new PriorityQueue<>((o1, o2) -> -o1.compareTo(o2));

        System.out.printf("\n== Run %s at %s with %d threads ==%n", instance, last, ForkJoinPool.getCommonPoolParallelism());
        GraphModel m = new GraphModel();
        disableVerification(m);
        declareGraph(m, instance);
        declareConstraints(m, instance);
        declareReachableIVconstraints(m, g);
        Solver solver = m.getSolver();
        try {
            solver.propagate();
        } catch (ContradictionException e) {
//            e.printStackTrace();
        }
//        for (int i = 0; i < 10; i++) print(reachableIV[i]);
        boolean solvewithtcut = ForkJoinPool.getCommonPoolParallelism() > 1;
        if (solvewithtcut) {
            gmatrix = matrix(g);
            dmatrix = matrix(doublements);
            int coupe = nodReg.get(source).round - 170;
            smartrecuts(coupe);
        }
        boolean solvewithoutcut = !solvewithtcut;
        if (solvewithoutcut) {
            solver.printShortFeatures();
            solver.setSearch(new GraphSearch(g).configure(GraphSearch.LEX).useLastConflict());
            ArrayList<int[]> sols = new ArrayList<>();
            int[] sol;
            while (solver.solve()) {
                sol = g.getMandPredOf(sink).toArray();
                soladd(sol, sols);//printsol(sol);
            }
            printsuper(sols);
            solver.printShortStatistics();
        }
    }

    /**
     * Declare Graph variable and other structures
     *
     * @param m        GraphModel
     * @param instance instance a resoudre
     */
    private void declareGraph(GraphModel m, Instance instance) {
        DirectedGraph glb = new DirectedGraph(m, n, dftSetType, false);
        DirectedGraph gub = new DirectedGraph(m, n, dftSetType, false);
        doublements = new DirectedGraph(m, n, dftSetType, false);
        longs = new DirectedGraph(m, n, dftSetType, false);
        loops = new DirectedGraph(m, n, dftSetType, false);
        // src is mandatory, that's the root node.
        source = lazyMakeNode(last.re, nbRound - last.ro);
        glb.addNode(source);
        gub.addNode(source);
        // a first loop just to declare nodes once for all
        toConnect.add(nodReg.get(source));
        while (!toConnect.isEmpty()) {
            connect(regNode.get(toConnect.poll()), gub, true);
        }
        List<Registre> regs = nodReg.valueCollection().stream().sorted((o1, o2) -> -o1.compareTo(o2)).collect(Collectors.toList());
        // then rewrite
        nodReg.clear();
        regNode.clear();
        for (int i = 0; i < regs.size(); i++) {
            Registre r = regs.get(i);
            r.setNode(i);
            regNode.put(r, i);
            nodReg.put(i, r);
        }
        // and fill the graph
        toConnect.add(nodReg.get(source));
        while (!toConnect.isEmpty()) {
            connect(regNode.get(toConnect.poll()), gub, false);
        }
        // Declare a sink node, the last round should be connected to this one.
        sink = currentNode++;
//        System.out.printf("Sink id:%d\n", sink);
        glb.addNode(sink);
        gub.addNode(sink);

        // Third
        connectLastRoundToSink(gub, Reg.A, sink, i -> i >= -80);
        connectLastRoundToSink(gub, Reg.B, sink, i -> i >= -80);
        connectLastRoundToSink(gub, Reg.C, sink, i -> i < -108);

        disconnectLastRoundToSink(gub, sink);
        int[] iv = instance.iv;
        IV = new HashSet<>();
        for (int i : iv) {
            Integer node = getNodeOrNull(Reg.B, -i);
            if (node != null) {
                Registre r = nodReg.get(node);
                glb.addNode(node);
                IV.add(node);
            }
        }
        g = m.digraphVar("trivium", glb, gub);
    }

    /**
     * Declare constraints on the Graph variable
     *
     * @param m        GraphModel
     * @param instance instance a resoudre
     */
    private void declareConstraints(GraphModel m, Instance instance) {
        int nbNodes = currentNode; // get max number of nodes
        // All node in solution must have at least one incomming arc exept the source
        declareMinInnerDegrees(g, nbNodes);
        // All node in solution must have one or two outcomming arc exept the sink
        // Variable storing the outcomming number of arcs of a node
        outDegrees = declareOuterDegrees(m, g, sink);

        SetVar activated = m.predSet(g, sink);
        activated.getCard().ge(instance.nAct).post();
        // Doubling arcs costraints
        doubleOrDie();
        // Three consecutive bits doubling pattern constraints
        triplectr(g, doublements, nodReg);

    }

    /**
     * Constraint all the three consecutive bits doubling pattern
     */
    public void triplectr(DirectedGraphVar gv, DirectedGraph doublements, TIntObjectHashMap<Registre> nodReg) {
        ArrayList<Pair<Integer, Integer>[]> triples = new ArrayList<>();
        for (int source : doublements.getNodes()) {
            if (doublements.getSuccOf(source).size() == 2) {
                int a0 = doublements.getSuccOf(source).min();
                int a1 = doublements.getSuccOf(source).max();
                if (nodReg.get(a0).round < nodReg.get(a1).round) {
                    int t = a0;
                    a0 = a1;
                    a1 = t;
                }
                if (doublements.getSuccOf(a0).size() == 2 && doublements.getSuccOf(a1).size() == 2) {
                    int b0 = doublements.getSuccOf(a0).min();
                    int b1 = doublements.getSuccOf(a0).max();
                    if (nodReg.get(b0).round < nodReg.get(b1).round) {
                        int t = b0;
                        b0 = b1;
                        b1 = t;
                    }
                    int b2 = doublements.getSuccOf(a1).min();
                    int b3 = doublements.getSuccOf(a1).max();
                    if (b2 == b1) b2 = b3;
                    else assert b3 == b1;
                    if (doublements.getSuccOf(b0).size() == 2 &&
                            doublements.getSuccOf(b1).size() == 2 &&
                            doublements.getSuccOf(b2).size() == 2) {
                        int c0 = doublements.getSuccOf(b0).min();
                        int c1 = doublements.getSuccOf(b0).max();
                        if (nodReg.get(c0).round < nodReg.get(c1).round) {
                            int t = c0;
                            c0 = c1;
                            c1 = t;
                        }
                        int c2 = doublements.getSuccOf(b2).min();
                        int c3 = doublements.getSuccOf(b2).max();
                        if (nodReg.get(c2).round < nodReg.get(c3).round) {
                            int t = c2;
                            c2 = c3;
                            c3 = t;
                        }
                        Pair<Integer, Integer>[] ctr1 = new Pair[]{
                                new Pair<>(b0, c0),
//                                new Pair<>(b0, c1),
                                new Pair<>(b2, c2),
//                                new Pair<>(b2, c3),
                                new Pair<>(b1, c1)
//                                new Pair<>(b1, c2)
                        };
                        Pair<Integer, Integer>[] ctr2 = new Pair[]{
                                new Pair<>(b0, c0),
//                                new Pair<>(b0, c1),
                                new Pair<>(b2, c2),
//                                new Pair<>(b2, c3),
                                new Pair<>(b1, c3)
                        };
                        triples.add(ctr1);
                        triples.add(ctr2);
                    }
                }

            }
        }

        ForbiddenArcsSolver fsol = new ForbiddenArcsSolver(gv);
        for (Pair<Integer, Integer>[] ctr : triples) fsol.addClause(ctr);
        new Constraint("triplies", fsol).post();
    }

    /**
     * declare the constraints for the cube reachability arrity
     */
    private void declareReachableIVconstraints(GraphModel mi, DirectedGraphVar gi) {
        reachableIV = new IntVar[sink + 1];
        reachableIV[sink] = mi.intVar("ari sink", 1);
        reachableIV[source] = mi.intVar("ari " + nodReg.get(source), instance.nAct, instance.nAct);
        for (int i : g.getPotPredOf(sink))
            if (IV.contains(i))
                reachableIV[i] = mi.intVar("ari " + nodReg.get(i), 1);
            else
                reachableIV[i] = mi.intVar("ari " + nodReg.get(i), 0);
        for (int i : g.getPotentialNodes())
            if (g.getPotSuccOf(i).size() == 0 && i != sink)
                reachableIV[i] = mi.intVar("ari " + nodReg.get(i), 0);
        for (int n : gi.getPotentialNodes())
            if (reachableIV[n] == null)
                reachableIV[n] = mi.intVar("ari " + nodReg.get(n), 0, 300);//T.get(n));
        new Constraint("reachable IV",
                new PropReachableIV(gi, doublements, longs, loops, reachableIV, nodReg, sink),
                new PropReachableIV2(gi, doublements, reachableIV, sink)).post();
    }

    /**
     * Soit ca double, soit ca ... double pas
     */
    private void doubleOrDie() {
        new Constraint("DOUBLEORDIE", new PropDoubleOrDie(g, doublements, outDegrees)).post();
    }

    /**
     * for any vertex i in g, |(j,i)| >= minDegree[i]
     * only holds on vertices that are mandatory
     */
    private void declareMinInnerDegrees(DirectedGraphVar g, int nbNodes) {
        int[] minDegrees = new int[nbNodes];
        for (int i : g.getPotentialNodes()) {
            minDegrees[i] = 1;
        }
        minDegrees[0] = 0;
        //m.minInDegrees(g, minDegrees).post();
        new Constraint("minInDegrees",
                new PropNodeDegree_AtLeast_Incr(g, Orientation.PREDECESSORS, minDegrees)).post();
    }

    /**
     * for any vertex i in g, |(i,j)| = degree[i]
     * only holds on vertices that are mandatory
     */
    private IntVar[] declareOuterDegrees(GraphModel m, DirectedGraphVar g, int sink) {
        IntVar[] degrees = new IntVar[g.getNbMaxNodes()];
        int[] minOutDegrees = new int[g.getNbMaxNodes()];
        Arrays.fill(degrees, m.intVar(0));
        for (int i : g.getPotentialNodes()) {
            minOutDegrees[i] = i == sink ? 0 : 1;
            degrees[i] = m.intVar("deg " + nodReg.get(i),
                    0,
                    Math.min(2, g.getPotSuccOf(i).size()));
        }
        new Constraint("minOutDegrees",
                new PropNodeDegree_AtLeast_Incr(g, Orientation.SUCCESSORS, minOutDegrees)).post();
        new Constraint("outDegrees", new PropNodeDegree_Var(g, Orientation.SUCCESSORS, degrees)).post();
        return degrees;
    }

    private void connectLastRoundToSink(DirectedGraph gub, Reg r, int sink, IntPredicate filter) {
        Integer node;
        for (int i = -1; i >= -r.siz; i--) {
            if ((node = getNodeOrNull(r, i)) != null && filter.test(i)) {
                gub.addArc(node, sink);
            }
        }
    }

    private void disconnectLastRoundToSink(DirectedGraph gub, int sink) {
        Reg r = Reg.B;
        Set<Integer> set = new HashSet<>();
        Arrays.stream(instance.iv).forEach(set::add);
        int[] C0 = IntStream.range(1, 81)
                .filter(i -> !set.contains(i))
                .toArray();
        Integer node;
        for (int i = 0; i < C0.length; i++) {
            if ((node = getNodeOrNull(r, -C0[i])) != null) {
                gub.removeArc(node, sink);
            }
        }
    }

    private int lazyMakeNode(Reg reg, int tour) {
        Registre r = new Registre(tour, reg);
        if (!regNode.containsKey(r)) {
            int n = currentNode++;
            r.setNode(n);
            regNode.put(r, n);
            nodReg.put(n, r);
        }
        return regNode.get(r);
    }

    private Integer getNodeOrNull(Reg reg, int tour) {
        Registre r = new Registre(tour, reg);
        if (!regNode.containsKey(r)) {
            return null;
        }
        return regNode.get(r);
    }

    private void connect(int node, DirectedGraph gub, boolean fake) {
        Registre r = nodReg.get(node);
        doConnect(r, gub, SHIFTS.get(r.reg).shift, SHIFTS.get(r.reg).regis, fake);
    }

    private void doConnect(Registre r, DirectedGraph gub, int[] D, Reg[] R, boolean fake) {
        int from = r.node;
        assert from != -1;
        if (r.round < 0) return;
        for (int i = 0; i < 5; i++) {
            if (r.round - D[i] >= -R[i].siz) {
                int node = lazyMakeNode(R[i], r.round - D[i]);
                toConnect.add(nodReg.get(node));
                if (fake) continue;
                gub.addNode(node);
                gub.addArc(from, node);
                if (i == 2) { // l'arc le plus long est toujours declare en 3ème position
                    longs.addArc(from, node);
                }
                if (i == 0) {
                    loops.addArc(from, node);
                }
            }
        }
        if (fake) return;
        if (r.round - D[3] >= -R[3].siz && r.round - D[4] >= -R[4].siz) {
            doublements.addArc(from, lazyMakeNode(R[3], r.round - D[3]));
            doublements.addArc(from, lazyMakeNode(R[4], r.round - D[4]));
        }
    }

    /**
     * Cut trivium then call solveorcut in parallel to solve the instance
     * @param coupe round number to cut
     */
    public void smartrecuts(int coupe) {
        long time = -System.currentTimeMillis();

        System.out.print("First cut at round " + coupe);
        Soljoin cuts = new Soljoin();
        solvesourcesubgraph(coupe, cuts);
        time += System.currentTimeMillis();
        cuts.filterdoublies();
        System.out.printf(", possible cuts: " + cuts.sols.size() + "/" + cuts.totalsol + " (" + cuts.doublons + " doublies)" + "  >>>> %.3fs%n", time / 1000f);

        Soljoin soljoin = new Soljoin();
        System.out.print("Dynamic recuts: ");
        if (debugPara) System.err.printf("Add 1: %d%n", remaining.addAndGet(cuts.sols.size()));

        time = -System.currentTimeMillis();
        List<RecursiveAction> actions = new ArrayList<>();
        for (int[] sol : cuts.sols) {
            actions.add(new RecursiveAction() {
                @Override
                protected void compute() {
                    solveorcut(sol, coupe- 56, IV, source, sink, nodReg, instance.nAct, doublements, longs, loops, soljoin);
                }
            });
        }
        ForkJoinTask.invokeAll(actions);
        time += System.currentTimeMillis();
        soljoin.filterdoublies();
        System.out.println(" = " + (soljoin.sols.size() + soljoin.doublons));
        System.out.printf(">>>>>> %.3fs %n", time / 1000f);

        printsuper(soljoin.sols);
    }

    /**
     * Build a graph model from a set of nodes, the cut, to the end of trivium
     * @param coupe cut nodes to start from
     * @return a Model from a cut to the end of trivium
     */
    public GraphModel buildcuttoend(GraphModel mi, DirectedGraph glb, DirectedGraph gub, int[] coupe, IntVar[] reachableIV,
                                    Set<Integer> IV, int source, int sink, TIntObjectHashMap<Registre> nodReg, int instancenAct,
                                    DirectedGraph doublements, DirectedGraph longs, DirectedGraph loops) {
        for (int n : coupe) {
            gub.addArc(source, n);
            glb.addArc(source, n);
            DirectedGraph sub = subgraph(n);
            for (int n2 : sub.getNodes())
                for (int s : sub.getSuccOf(n2))
                    gub.addArc(n2, s);
        }
        for (int n : IV) gub.addArc(n, sink);
        for (int n : IV) glb.addArc(n, sink);
        DirectedGraphVar gi = mi.digraphVar("triviumafcuttoend", glb, gub);

        int[] minDegrees = new int[gi.getNbMaxNodes()];
        for (int i : gi.getPotentialNodes()) minDegrees[i] = 1;
        minDegrees[source] = 0;
        IntVar[] degrees = new IntVar[gi.getNbMaxNodes()];
        int[] minOutDegrees = new int[gi.getNbMaxNodes()];
        Arrays.fill(degrees, mi.intVar(0));
        for (int i : gi.getPotentialNodes()) {
            minOutDegrees[i] = i == sink ? 0 : 1;
            minOutDegrees[source] = coupe.length;
            if (i == source)
                degrees[i] = mi.intVar("deg " + nodReg.get(i), coupe.length, coupe.length);
            else
                degrees[i] = mi.intVar("deg " + nodReg.get(i), 0, Math.min(2, gi.getPotSuccOf(i).size()));
        }
        new Constraint("minInDegrees", new PropNodeDegree_AtLeast_Incr(gi, Orientation.PREDECESSORS, minDegrees)).post();
        new Constraint("minOutDegrees", new PropNodeDegree_AtLeast_Incr(gi, Orientation.SUCCESSORS, minOutDegrees)).post();
        new Constraint("outDegrees", new PropNodeDegree_Var(gi, Orientation.SUCCESSORS, degrees)).post();
        new Constraint("DOUBLEORDIE", new PropDoubleOrDieExeptSource(gi, doublements, degrees)).post();
//        new Constraint("doubling pattern", new PropDoublePattern(gi, doublements, longs, degrees)).post();

        reachableIV[sink] = mi.intVar("ari sink", 1);
        reachableIV[source] = mi.intVar("ari " + nodReg.get(0), instancenAct, instancenAct);
        for (int i : gi.getPotPredOf(sink))
            if (IV.contains(i))
                reachableIV[i] = mi.intVar("ari " + nodReg.get(i), 1);
            else
                reachableIV[i] = mi.intVar("ari " + nodReg.get(i), 0);
        for (int i : gi.getPotentialNodes())
            if (gi.getPotSuccOf(i).size() == 0 && i != sink)
                reachableIV[i] = mi.intVar("ari " + nodReg.get(i), 0);
        for (int n : gi.getPotentialNodes())
            if (reachableIV[n] == null)
                reachableIV[n] = mi.intVar("ari " + nodReg.get(n), 0, 300);
        new Constraint("reachable IV",
                new PropReachableIVExeptSource(gi, doublements, longs, loops, reachableIV, nodReg, sink),
                new PropReachableIV2ExeptSource(gi, doublements, longs, loops, reachableIV, nodReg, sink)).post();
        triplectr(gi, doublements, nodReg);
        disableVerification(mi);
        mi.getSolver().setSearch(new GraphSearch(gi).configure(GraphSearch.LEX).useLastConflict());
        return mi;
    }

    /**
     * Build a graph model from a set of nodes, the cut, to a round number
     * @param coupe cut nodes to start from
     * @param recoupe round to recut the model
     * @return a Model from a cut to the end of trivium
     */
    public GraphModel buildcuttocut(GraphModel mi, DirectedGraph glb, DirectedGraph gub, int[] coupe, int recoupe,
                                    int source, int sink, TIntObjectHashMap<Registre> nodReg, DirectedGraph doublements, DirectedGraph longs) {
        DirectedGraph ins = new DirectedGraph(n, dftSetType, false);
        Deque<Integer> list = new ArrayDeque<>();
        for (int n : coupe) {
            list.add(n);
            ins.addNode(n);
        }
        Registre reg;
        while (!list.isEmpty()) {
            int j = list.pop();
            reg = nodReg.get(j);
            if (reg.round > recoupe)
                for (int s = gmatrix[j].nextSetBit(0); s > -1; s = gmatrix[j].nextSetBit(s + 1)) {
                    ins.addArc(j, s);
                    list.add(s);
                }
        }
        glb.addNode(source);
        gub.addNode(source);
        for (int n : coupe) {
            gub.addArc(source, n);
            glb.addArc(source, n);
        }
        for (int n : ins.getNodes())
            for (int s : ins.getSuccOf(n))
                gub.addArc(n, s);
        glb.addNode(sink);
        gub.addNode(sink);
        for (int i : ins.getNodes())
            if (ins.getSuccOf(i).size() == 0)
                gub.addArc(i, sink);
        DirectedGraphVar gi = mi.digraphVar("triviumcutcut", glb, gub);
        int[] minDegrees = new int[gi.getNbMaxNodes()];
        for (int i : gi.getPotentialNodes())
            minDegrees[i] = 1;
        minDegrees[source] = 0;
        IntVar[] degrees = new IntVar[gi.getNbMaxNodes()];
        int[] minOutDegrees = new int[gi.getNbMaxNodes()];
        Arrays.fill(degrees, mi.intVar(0));
        for (int i : gi.getPotentialNodes()) {
            minOutDegrees[i] = i == sink ? 0 : 1;
            minOutDegrees[source] = coupe.length;
            if (i == source)
                degrees[i] = mi.intVar("deg " + nodReg.get(i), coupe.length, coupe.length);
            else
                degrees[i] = mi.intVar("deg " + nodReg.get(i), 0, Math.min(2, gi.getPotSuccOf(i).size()));
        }
        new Constraint("minInDegrees", new PropNodeDegree_AtLeast_Incr(gi, Orientation.PREDECESSORS, minDegrees)).post();
        new Constraint("minOutDegrees", new PropNodeDegree_AtLeast_Incr(gi, Orientation.SUCCESSORS, minOutDegrees)).post();
        new Constraint("outDegrees", new PropNodeDegree_Var(gi, Orientation.SUCCESSORS, degrees)).post();
        new Constraint("DOUBLEORDIE", new PropDoubleOrDieExeptSource(gi, doublements, degrees)).post();

        triplectr(gi, doublements, nodReg);
        disableVerification(mi);
        mi.getSolver().setSearch(new GraphSearch(gi).configure(GraphSearch.LEX).useLastConflict());
        return mi;
    }
    /**
     * Build a graph model from a set of nodes, the cut, to either the end of trivium or an other cut round. The choice to recut is made with the parameter isrecutable
     * @param coupe cut nodes to start from
     * @param recoupe round to recut the model if needed
     * @param soljoin All parallel solutions are joined in Soljoin
     */
    public void solveorcut(int[] coupe, int recoupe,
                           Set<Integer> IV, int source, int sink, TIntObjectHashMap<Registre> nodReg, int instancenAct,
                           DirectedGraph doublements, DirectedGraph longs, DirectedGraph loops, Soljoin soljoin) {
        if (debugPara) System.err.printf("+%d <- %d%n", active.incrementAndGet(), remaining.decrementAndGet());
        GraphModel mi = new GraphModel();
        DirectedGraph glb = new DirectedGraph(mi, sink + 1, dftSetType, false);
        DirectedGraph gub = new DirectedGraph(mi, sink + 1, dftSetType, false);
        IntVar[] reachableIV = new IntVar[sink + 1];
        mi = buildcuttoend(mi, glb, gub, coupe, reachableIV, IV, source, sink, nodReg, instancenAct, dcopy(), longs, loops);
        try {
            mi.getSolver().propagate();

            int[] sol = new int[0];
            ArrayList<int[]> sols = new ArrayList<>();

            int arritylimit = instancenAct + 10;//4 * (this.reachableIV[0].getUB() - instancenAct) / 8 + instancenAct;
            boolean isrecutable =
                    arritycut(coupe, reachableIV) > arritylimit &&
                            recoupe > 300 && coupe.length < 8;// + 15 * (arritylimit - arritycut(coupe, reachableIV));

            if (!isrecutable) {
                while (mi.getSolver().solve()) {
                    sol = glb.getPredOf(sink).toArray();
                    sols.add(sol);
                }
                if (sol.length > 1) {
                    System.out.println("+" + mi.getSolver().getSolutionCount() + " solutions (" + (long) mi.getSolver().getTimeCount() + "s)");
                    synchronized (soljoin) {
                        soljoin.soladdall(sols);
                    }
                }
                if (debugPara) active.decrementAndGet();
                if (mi.getSolver().getTimeCount() > 10.)
                    System.out.println("~" + ForkJoinTask.getSurplusQueuedTaskCount() + " tasks left, uncut of size " + coupe.length + " arity " + arritycut(coupe, reachableIV) + " (" + (long) mi.getSolver().getTimeCount() + "s " + mi.getSolver().getSolutionCount() + " sols)");
            } else {
                mi = new GraphModel();
                glb = new DirectedGraph(mi, sink + 1, dftSetType, false);
                gub = new DirectedGraph(mi, sink + 1, dftSetType, false);
                mi = buildcuttocut(mi, glb, gub, coupe, recoupe, source, sink, nodReg, dcopy(), longs);

                ArrayList<int[]> recuts = new ArrayList<>();
                while (mi.getSolver().solve()) {
                    sol = glb.getPredOf(sink).toArray();
                    if (arritycut(sol, reachableIV) >= instancenAct)
                        soladd(sol, recuts);
                }
                if (recuts.size() > 1) {
                    if (debugPara) remaining.addAndGet(recuts.size());
                    if (debugPara) active.decrementAndGet();
                    System.out.println("+" + recuts.size() + " recuts (" + (long) mi.getSolver().getTimeCount() + "s)");
                    List<RecursiveAction> actions = new ArrayList<>();
                    for (int[] recut : recuts) {
                        actions.add(new RecursiveAction() {
                            @Override
                            protected void compute() {
                                solveorcut(recut, recoupe - 56, IV, source, sink, nodReg, instancenAct, doublements, longs, loops, soljoin);
                            }
                        });
                    }
                    ForkJoinTask.invokeAll(actions);
                } else {
                    //when the recut recuts nothing, the cut is solved normaly to prevent infinite recuts.
                    System.out.println("inefficient recut");
                    solvesaftercut(coupe, IV, source, sink, nodReg, instancenAct, dcopy(), longs, loops, soljoin);
                    if (debugPara) active.decrementAndGet();
                }
            }
        } catch (ContradictionException ignored) {
            if (debugPara) active.decrementAndGet();
        }
    }

    /**
     * solve all solutions until a certain round
     *
     * @param cut round limit
     */
    public void solvesourcesubgraph(int cut, Soljoin soljoin) {
        DirectedGraph ins = sourcesubgraph(cut);
        GraphModel mi = new GraphModel();
        DirectedGraph glb = new DirectedGraph(mi, n, dftSetType, false);
        DirectedGraph gub = new DirectedGraph(mi, n, dftSetType, false);
        glb.addNode(source);
        gub.addNode(source);
        for (int n : ins.getNodes())
            for (int s : ins.getSuccOf(n))
                gub.addArc(n, s);
        glb.addNode(sink);
        gub.addNode(sink);
        for (int i : ins.getNodes())
            if (ins.getSuccOf(i).size() == 0)
                gub.addArc(i, sink);
        DirectedGraphVar gi = mi.digraphVar("triviumsource", glb, gub);

        int[] minDegrees = new int[gi.getNbMaxNodes()];
        for (int i : gi.getPotentialNodes()) minDegrees[i] = 1;
        minDegrees[source] = 0;
        IntVar[] degrees = new IntVar[gi.getNbMaxNodes()];
        int[] minOutDegrees = new int[gi.getNbMaxNodes()];
        Arrays.fill(degrees, mi.intVar(0));
        for (int i : gi.getPotentialNodes()) {
            minOutDegrees[i] = i == sink ? 0 : 1;
            degrees[i] = mi.intVar("deg " + nodReg.get(i), 0, Math.min(2, gi.getPotSuccOf(i).size()));
        }
        new Constraint("minInDegrees", new PropNodeDegree_AtLeast_Incr(gi, Orientation.PREDECESSORS, minDegrees)).post();
        new Constraint("minOutDegrees", new PropNodeDegree_AtLeast_Incr(gi, Orientation.SUCCESSORS, minOutDegrees)).post();
        new Constraint("outDegrees", new PropNodeDegree_Var(gi, Orientation.SUCCESSORS, degrees)).post();
        new Constraint("DOUBLEORDIE", new PropDoubleOrDie(gi, doublements, degrees)).post();
        triplectr(gi, doublements, nodReg);
        disableVerification(mi);
        mi.getSolver().setSearch(new GraphSearch(gi).configure(GraphSearch.LEX).useLastConflict());
        int[] sol;
        int sumIV;
        while (mi.getSolver().solve()) {
            sol = glb.getPredOf(sink).toArray();
            sumIV = 0;
            for (int n : sol)
                sumIV += reachableIV[n].getUB();
            if (sumIV >= instance.nAct) soljoin.soladd(sol);
        }
        if (mi.getSolver().getSolutionCount() > 0)
            soljoin.totalsol += mi.getSolver().getSolutionCount();
    }

    /**
     * solve a submodel from a cut to the sink
     *
     * @param coupe set of starting nodes
     */
    public void solvesaftercut(int[] coupe,
                               Set<Integer> IV, int source, int sink, TIntObjectHashMap<Registre> nodReg, int instancenAct,
                               DirectedGraph doublements, DirectedGraph longs, DirectedGraph loops, Soljoin soljoin) {
        GraphModel mi = new GraphModel();
        DirectedGraph glb = new DirectedGraph(mi, sink + 1, dftSetType, false);
        DirectedGraph gub = new DirectedGraph(mi, sink + 1, dftSetType, false);
        IntVar[] reachableIV = new IntVar[sink + 1];
        mi = buildcuttoend(mi, glb, gub, coupe, reachableIV, IV, source, sink, nodReg, instancenAct, dcopy(), longs, loops);
        int[] sol = new int[0];
        ArrayList<int[]> sols = new ArrayList<>();
        while (mi.getSolver().solve()) {
            sol = glb.getPredOf(sink).toArray();
            sols.add(sol);
        }
        if (sol.length > 1) {// || mi.getSolver().getTimeCount() > 1.) {
            System.out.print("+" + mi.getSolver().getSolutionCount());
            synchronized (soljoin) {
                soljoin.soladdall(sols);
            }
        }
//        if (mi.getSolver().getTimeCount() > 10.)
//            print("innefficient taille " + coupe.length + " arrité " + arritycut(coupe, reachableIV));
    }

    private BitSet[] matrix(DirectedGraph g) {
        BitSet[] matrix = new BitSet[sink + 1];
        for (int n : g.getNodes()) {
            matrix[n] = new BitSet(sink + 1);
            for (int s : g.getSuccOf(n)) {
                matrix[n].set(s);
            }
        }
        return matrix;
    }

    private BitSet[] matrix(DirectedGraphVar g) {
        BitSet[] matrix = new BitSet[sink + 1];
        for (int n : g.getPotentialNodes()) {
            matrix[n] = new BitSet(sink + 1);
            for (int s : g.getPotSuccOf(n)) {
                matrix[n].set(s);
            }
        }
        return matrix;
    }

    private DirectedGraph subgraph(int i) {
        DirectedGraph sub = new DirectedGraph(nbRound * 4, dftSetType, false);
        sub.addNode(i);
        Deque<Integer> list = new ArrayDeque<>();
        list.add(i);
        while (!list.isEmpty()) {
            int j = list.pop();
            if (gmatrix[j] != null)
                for (int s = gmatrix[j].nextSetBit(0); s > -1; s = gmatrix[j].nextSetBit(s + 1)) {
                    sub.addArc(j, s);
                    list.add(s);
                }
        }
        return sub;
    }

    public DirectedGraph sourcesubgraph(int r) {
        DirectedGraph gs = new DirectedGraph(n, dftSetType, false);
        gs.addNode(source);

        Deque<Integer> list = new ArrayDeque<>();
        list.add(source);
        Registre reg;
        while (!list.isEmpty()) {
            int j = list.pop();
            reg = nodReg.get(j);
            if (reg.round > r) {
                for (int s : g.getPotSuccOf(j)) {
                    gs.addArc(j, s);
                    list.add(s);
                }
            }
        }
        return gs;
    }

    public int arritycut(int[] cut, IntVar[] reachableIV) {
        int sumIV = 0;
        for (int n : cut)
            sumIV += reachableIV[n].getUB();
        return sumIV;
    }

    private DirectedGraph dcopy() {
        DirectedGraph c = new DirectedGraph(n, dftSetType, false);
        for (int i = 0; i < sink; i++) {
            if (dmatrix[i] != null)
                for (int s = dmatrix[i].nextSetBit(0); s > -1; s = dmatrix[i].nextSetBit(s + 1))
                    c.addArc(i, s);
        }
        return c;
    }

    /**
     * Disable the verification after the choco resolution (reduce solving time)
     *
     * @param m model
     */
    private void disableVerification(Model m) {
        m.set(new Settings() {
            @Override
            public AbstractStrategy makeDefaultSearch(Model model) {
                // overrides default search strategy to handle graph vars
                AbstractStrategy other = Search.defaultSearch(model);
                GraphVar[] gvs = ((GraphModel) model).retrieveGraphVars();
                if (gvs.length == 0) {
                    return other;
                } else {
                    AbstractStrategy[] gss = new AbstractStrategy[gvs.length + 1];
                    for (int i = 0; i < gvs.length; i++) {
                        gss[i] = new GraphStrategy(gvs[i]);
                    }
                    gss[gvs.length] = other;
                    return Search.sequencer(gss);
                }
            }

            @Override
            public boolean checkModel(Solver solver) {
                return true;
            }
        });
    }

    private void soladd(int[] sol, ArrayList<int[]> sols) {
        int[] removesol = new int[0];
        for (int[] sol2 : sols)
            if (sol2.length == sol.length) {
                boolean eq = true;
                for (int i : sol) {
                    boolean in = false;
                    for (int i2 : sol2)
                        in = in || i == i2;
                    eq = eq && in;
                }
                if (eq) removesol = sol2;
            }
        if (removesol.length == 0) sols.add(sol);
        else sols.remove(removesol);
    }

    /**
     * Print the superpoly
     * @param sols
     */
    private void printsuper(ArrayList<int[]> sols) {
        if (sols.size() > 0) {
            int[][] solsint = new int[sols.size()][];
            Arrays.setAll(solsint, i -> sols.get(i));
            fusionSort(solsint, 0, solsint.length - 1);
            System.out.print("\nSUPERPOLY: ");
            for (int[] sol : solsint) printsol(sol);
            System.out.println("\n");
        }
    }

    /**
     * Print a solution
     * @param sol
     */
    private void printsol(int[] sol) {
        List<String> vars = new ArrayList<String>();
        boolean isIV = true;
        for (int p : sol) {
            if (!IV.contains(p)) {
                Registre r = nodReg.get(p);
                isIV = false;
                vars.add((r.reg == Reg.A ? "x" : r.reg == Reg.B ? "v" : "z") + -r.round);
            }
        }
        vars.sort(String.CASE_INSENSITIVE_ORDER);
        if (isIV) vars.add("1");
        for (String var : vars)
            System.out.print(var);
        System.out.print(" ");
    }

    /**
     * Last possible bits of trivium
     */
    private enum Last {
//        a1(Reg.A, 1),
        a66(Reg.A, 66),
        a93(Reg.A, 93),
        b69(Reg.B, 69),
        b84(Reg.B, 84),
        c66(Reg.C, 66),
        c111(Reg.C, 111),
        ;
        Reg re;
        int ro;

        Last(Reg a, int i) {
            this.re = a;
            this.ro = i;
        }
    }

    private enum Instance {
        iv_100(new int[]{}, 0),
        iv_500(new int[]{1, 2, 3, 4, 5, 6}, 6),
        iv_570(new int[]{1, 11, 21, 31, 41, 51, 61}, 7),
        iv_590(new int[]{1, 11, 21, 31, 41, 51, 61, 71}, 8),
        iv_591(new int[]{1, 11, 21, 31, 41, 51, 61, 71}, 8),

        iv_672_1(new int[]{4, 14, 19, 27, 39, 41, 48, 50, 56, 58, 67, 80}, 12), // 1+ x2 + x26 dans c66
        iv_672_2(new int[]{3, 6, 8, 11, 15, 25, 28, 40, 50, 57, 58, 62}, 12), // 1+x15 dans c66
        iv_672_3(new int[]{5, 10, 11, 16, 22, 25, 33, 37, 38, 49, 53, 74}, 12), // x20 dans c111
        iv_672_4(new int[]{4, 6, 15, 17, 19, 21, 34, 57, 58, 66, 74, 76}, 12), //1+x25 dans c66
        iv_672_5(new int[]{5, 11, 16, 22, 25, 29, 33, 49, 50, 65, 72, 74}, 12), // x33 dans c66
        iv_672_6(new int[]{4, 15, 17, 19, 21, 24, 33, 47, 57, 58, 66, 74}, 12), // x45 + x63 dans c66

        iv_675_1(new int[]{3, 14, 21, 25, 38, 43, 44, 47, 54, 56, 58, 68}, 12), // 1+ x1 + x10 + x51  (1+x1+x10 dans b84 et x51 dans c66)
        iv_675_2(new int[]{2, 4, 7, 8, 13, 19, 23, 39, 48, 59, 68, 75}, 12), //x8 dans b84
        iv_675_3(new int[]{2, 7, 8, 13, 19, 22, 30, 34, 35, 46, 50, 71}, 12), // x17 dans c111
        iv_675_4(new int[]{12, 17, 21, 23, 36, 44, 47, 52, 56, 59, 63, 64}, 12), // 1 + x21 + x51 dans c66

        iv_681_1(new int[]{6, 8, 11, 14, 16, 18, 29, 41, 48, 74, 77, 80}, 12), // x61
        iv_685_1(new int[]{1, 3, 10, 12, 14, 38, 45, 48, 50, 69, 75, 79}, 12), // x16

        iv_735_1(new int[]{2, 5, 9, 12, 13, 14, 19, 28, 36, 38, 40, 47, 49, 51, 52, 53, 55, 57, 63, 64, 66, 73, 79}, 23), //1 + x1 dans a66
        iv_735_2(new int[]{3, 4, 6, 9, 16, 17, 23, 27, 29, 38, 41, 46, 51, 56, 62, 63, 65, 66, 71, 72, 75, 78, 80}, 23), //x5
        iv_735_3(new int[]{5, 8, 12, 13, 15, 18, 19, 23, 25, 31, 34, 38, 39, 41, 51, 53, 64, 65, 67, 71, 73, 75, 78}, 23), //x9 dans a66


        iv_840_1(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,/*34,*/35, 36, 37, 38, 39,
                40, 41, 42, 43, 44, 45, 46,/*47,*/48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
                60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80}, 78),
        iv_840_2(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,/*34,*/35, 36, 37, 38, 39,
                40, 41, 42, 43, 44, 45, 46,/*47,*/48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
                60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, /*71,*/ 72, /*73,*/ 74, /*75,*/ 76, /*77,*/ 78, /*79,*/ 80}, 78),
        iv_840_3(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
                40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
                60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, /*73,*/ 74, /*75,*/ 76, /*77,*/ 78, /*79,*/ 80}, 78);
        int[] iv;
        int nAct;

        Instance(int[] iv, int nAct) {
            this.iv = iv;
            this.nAct = nAct;
        }

    }

    public static void main(String[] args) throws CmdLineException {
        if (args.length < 6) {
            new TriviumGraph(args[0], args[1], args[2], args[3], "-b", "a66");
            new TriviumGraph(args[0], args[1], args[2], args[3], "-b", "a93");
            new TriviumGraph(args[0], args[1], args[2], args[3], "-b", "b69");
            new TriviumGraph(args[0], args[1], args[2], args[3], "-b", "b84");
            new TriviumGraph(args[0], args[1], args[2], args[3], "-b", "c66");
            new TriviumGraph(args[0], args[1], args[2], args[3], "-b", "c111");
        } else {
            new TriviumGraph(args);
        }
    }

    public static boolean compare(int[] s1, int[] s2) {
        if (s1.length > s2.length) return true;
        if (s1.length == s2.length) {
            for (int i = 0; i < s1.length; i++) {
                if (s1[i] > s2[i]) return true;
                if (s1[i] < s2[i]) return false;
            }
        }
        return false;
    }

    public static void fusion(int[][] t, int d, int m, int f) {
        int[][] tmp = new int[f - d + 1][];//tab temporaire trié
        int di = d;//Indice courant partie gauche
        int fi = m + 1;//Indice courant partie droite
        for (int i = 0; i < tmp.length; i++) {
            if (fi > f) tmp[i] = t[di++];//Partie droite finie
            else if (di > m) tmp[i] = t[fi++];//Partie gauche finie
            else if (compare(t[di], t[fi])) tmp[i] = t[fi++];
            else tmp[i] = t[di++];
        }
        for (int i = 0; i < tmp.length; i++) {//recopie du tableau trié
            t[d + i] = tmp[i];
        }
    }

    public static void fusionSort(int[][] t, int d, int f) {
        if (d == f) return;
        int m = (f - d) / 2 + d;
        fusionSort(t, d, m);
        fusionSort(t, m + 1, f);
        fusion(t, d, m, f);
    }
}